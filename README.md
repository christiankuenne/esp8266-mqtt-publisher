# ESP8266 MQTT Publisher

Hello world example to publish messages from an ESP8266 device.

## Prerequisites

- [PlatformIO](https://platformio.org/) (VS Code Extension)

If you want to run the project outside of VS Code, you can install PlatformIO as described [here](https://docs.platformio.org/en/latest/core/installation.html).

## Getting started

Configure `SSID`, `PSK` and `MQTT_BROKER` in [main.cpp](src/main.cpp).

- `make build`: Build the project
- `make upload`: Upload to device
- `make monitor`: Connect to device and monitor output
- `make clean`: Clean the build environment

Run an MQTT server to see the messages:

```
docker run -it -p 1883:1883 eclipse-mosquitto
mosquitto_sub -v -h URL_HERE -t 'home/test'
```
